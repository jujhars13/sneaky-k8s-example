// libs scanned by vuln cost
const axios = require("axios");
const mongo = require("mongodb").MongoClient;

// ideally we'd use the adapter pattern and be able to abstract out different API endpoints
// in case this one's down or rate-limited - no time
const BITCOIN_API = "https://api.coindesk.com/v1/bpi/currentprice.json";
const {
  NODE_ENV = "development",
  MONGO_HOST = undefined,
  MONGO_PORT = 27017,
} = process.env;

if (MONGO_HOST === undefined) {
  throw new RangeError("MONGO_HOST must be defined");
}

const mongoConnectionString = `mongodb://${MONGO_HOST}:${MONGO_PORT}/bitcoin`;
const client = new mongo(mongoConnectionString, { useUnifiedTopology: true });

console.log("Starting Bitcoin fetch");

// fetch latest value from API
axios
  .get(BITCOIN_API)
  .then((response) => {
    if (!response.data.bpi.USD.rate) {
      throw new Error(`proper data failed to return from ${BITCOIN_API}`);
    }
    console.debug("raw data", response.data);
    return response.data;
  })
  // Write to db
  .then((data) => {
    console.log(`Current USD rate is ${data.bpi.USD.rate}, writing to db`);
    return writeToDb(client, { data, updated: data.time.updatedISO });
  })
  .then(() => {
    console.log("Finished Bitcoin fetch and save");
    process.exit(0);
  })
  .catch((error) => {
    console.error(error);
    process.exit(99);
  });

/**
 * Write to mongodb, pulled out here to keep promise chain easier to read
 * @param {obj} mongoClient mongo client object DI
 * @param {obj} data raw bitcoin api response as a obj https://api.coindesk.com/v1/bpi/currentprice.json
 */
function writeToDb(mongoClient, data) {
  return (
    mongoClient
      .connect()
      .then((db) => {
        let database = mongoClient.db("bitcoin");
        let collection = database.collection("rate");
        return collection.insertOne(data);
      })
      // just rethrow error to bomb out here
      .catch((err) => {
        console.error(err);
        throw new Error(err);
      })
  );
}
