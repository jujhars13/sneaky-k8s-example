// libs scanned by vuln cost
// allow express.js, let's just keep this straightforward
const http = require("http");
const mongo = require("mongodb").MongoClient;
const { version } = require("./package.json");

const {
  NODE_ENV = "development",
  MONGO_HOST = undefined,
  MONGO_PORT = 27017,
  NODE_PORT = 8080,
} = process.env;

if (MONGO_HOST === undefined) {
  throw new RangeError("MONGO_HOST must be defined");
}

const mongoConnectionString = `mongodb://${MONGO_HOST}:${MONGO_PORT}/bitcoin`;
const client = new mongo(mongoConnectionString, { useUnifiedTopology: true });

/**
 * pure native http - no framework based HTTP response handler
 *
 * routes are:
 *  /
 *  /average
 *  /healthcheck which doesn't hit our DB and create load
 *
 * @param {obj} request
 * @param {obj} response
 */
const requestHandler = (request, response) => {
  console.log(
    `${new Date()} ${request.connection.remoteAddress} accessed ${
      request.method
    }:${request.url} [${NODE_ENV}]`
  );

  let out = { version, name: "Bitcoin api", environment: NODE_ENV };
  response.setHeader("Content-Type", "application/json");
  response.statusCode = 200;

  // old skool router
  // ROUTE: /healthcheck
  if (request.url === "/healthcheck" && request.method === "GET") {
    out.data = { message: "ok" };
    response.end(JSON.stringify(out));
  }

  // ROUTE: /
  if (request.url === "/" && request.method === "GET") {
    out.data = { rate: "undefined" };
    return getValuesFromDb(client)
      .then((data) => {
        if (!data[0]) {
          out.data = {};
          throw new Error("no data returned from db");
        }
        out.data = {
          rate: parseFloat(data[0].data.bpi.USD.rate.replace(",", "")),
          updated: data[0].data.time.updateduk,
        };
      })
      .then(() => response.end(JSON.stringify(out)))
      .catch((error) => {
        console.error(error);
        response.statusCode = 500;
        response.end(JSON.stringify({ error }));
      });
  }

  // ROUTE: /average
  if (request.url === "/average" && request.method === "GET") {
    out.data = { rate: "undefined" };
    return getValuesFromDb(client, 10)
      .then((data) => {
        if (!data[0]) {
          out.data = {};
          throw new Error("no data returned from db");
        }

        // work out average of last "objSize" values
        let sampleSize = 0;
        let sum = data.reduce((acc, cur) => {
          let val = parseFloat(cur.data.bpi.USD.rate.replace(",", ""));
          sampleSize++;
          return acc + val;
        }, 0);

        let average = sum / sampleSize;

        out.data = { average, sampleSize };
      })
      .then(() => response.end(JSON.stringify(out)))
      .catch((error) => {
        console.error(error);
      });
  }
};

const server = http.createServer(requestHandler);
server.listen(NODE_PORT, (err) => {
  if (err) {
    console.error(err);
    process.exit(22);
  }
  console.log(
    `server is listening on ${NODE_PORT} [${NODE_ENV}], healthcheck on /healthcheck`
  );
});

/**
 * get values from Mongo db, pulled out here to keep promise chain easier to read
 * @param {obj} mongoClient mongo client object DI
 * @param {int} limit how many records to return DEFAULT=1
 */
function getValuesFromDb(mongoClient, limit = 1) {
  return (
    mongoClient
      .connect()
      .then((db) => {
        const database = mongoClient.db("bitcoin");
        const collection = database.collection("rate");
        // natural sort by -1 will return newest records first
        return collection.find().sort({ $natural: -1 }).limit(limit).toArray();
      })
      // just rethrow error to bomb out here
      .catch((err) => {
        console.error(err);
        throw new Error(err);
      })
  );
}
