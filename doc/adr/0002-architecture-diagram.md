# 2. Architecture Diagram

Date: 2020-10-31

## Status

Accepted

## Context

Here's the architecture diagram for the target state

## Decision

![0002-architecture-diagram.png](0002-architecture-diagram.png)
