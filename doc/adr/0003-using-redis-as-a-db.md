# 3. using redis as a db

Date: 2020-10-31

## Status

Superceded by [5. pivoting to mongo](0005-pivoting-to-mongo.md)

## Context

What datastore should we use?
Relational, Time Series, Document, Key Value

## Decision

We're go with Redis as it's very easy to get going

## Consequences

We can't do more complex things
