# 4. pivoting to mongo

Date: 2020-10-31

## Status

Accepted

Supercedes [3. using redis as a db](0003-using-redis-as-a-db.md)

## Context

We were previously using Redis and was thinking of using a queue to push/pop
Not thinking it through though, we need old values to stay in the db so we can work out an avg of previous 10 values. Popping a value off of our queue would destroy it

A time series DB would be perfect in theory, but don't know enough about them and can't be bothered to create a schema for MySQL/Posgresql/Sqlite

## Decision

Switching out to Mongo

## Consequences

We will keep historical values and reading them is idempotent
