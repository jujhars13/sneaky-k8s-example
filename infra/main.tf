// @see variables.tf

provider "google" {
  project = var.project
  region  = var.region

}

resource "google_container_cluster" "primary_cluster" {
  name     = var.cluster_name
  location = var.region
  min_master_version = "1.17"

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1
  description              = "sneaky cluster"
  #network ="" TODO create custom VPC

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  timeouts {
    create = "10m"
    update = "20m"
  }
}

# use pre-emtable because I'm tight
resource "google_container_node_pool" "primary_cluster_preemptible_nodes" {
  name               = "${var.cluster_name}-nodes"
  location           = var.region
  cluster            = google_container_cluster.primary_cluster.name
  node_count         = 1

  node_config {
    preemptible  = true
    machine_type = "e2-medium"
    disk_size_gb = "100"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }

  timeouts {
    create = "10m"
    update = "20m"
  }
}
