variable "project" {
  type        = string
  description = "the GCP region"
  default   = "europe-west1"
}

variable "region" {
  type        = string
  description = "the GCP region"
}

variable "cluster_name" {
  type        = string
  description = "the k8s cluster_name you want"
}
