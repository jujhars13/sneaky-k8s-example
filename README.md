# Sneaky K8s Example

Messing around with K8s and RBAC.

## Spec

1. Create a K8s cluster somewhere w/RBAC
2. Create 2 services, one **Frontend A**, one **Worker B** to get data from an external, one **Api C** and **Datastore D**
3. Deploy some sort of data store to store data in the db
4. Ensure that **Frontend A** can only talk to the **Api C** and nothing else
5. Worker B gets current bitcoin dollar value from <somewhere> every minute and stores it in

## ADRs - Architectural Decision Records

[Architectural Decision Records](https://www.thoughtworks.com/radar/techniques/lightweight-architecture-decision-records) are stored in `doc/adr` and we're using [adr-tools](https://github.com/npryce/adr-tools) to help generate them.
We use ADRs as an immutable, collaborative document to record our technical decision making.
![0002-architecture-diagram.png](doc/adr/0002-architecture-diagram.png)

## Assumptions/Tech Debt

These are the assumptions I'm making but hopefully giving my an indication to my thought processes in a real prod environment.

1. Front end service will be a dirty jquery-esq front end, no SPA React-y magic here - I'm just a 2/3rds stack developer
2. There will be no authentication between **Frontend A**  and **Api C**
3. Will be using vanilla Node.js 14 LTS as runtime of choice
4. Probably use Mongo as a quick and dirty db w/ no auth rather an a structured/timeseries db
5. To keep things quick **Worker B** will write directly to datastore rather than POST to **Api C** which is the better pattern
6. Will probably get K8s cluster created via bash first then re-write in Terraform
7. If I get time I'll re-write k8s manifests in [Kustomize](https://kustomize.io/) as helm is now old-skool. In the meantime expect some bash-fu instead
8. Any container images will be public
9. **Api C** Will also be publicly exposed to demonstrate ingress controller routing
10. **Frontend A** will not be able to talk to **Worker B** or **Datastore C** protected via RBAC
11. Won't spend time on things like:
    1.  CI/CD
    2.  Linting and coding standards, will try to be as ESlint-y and shellcheck-y as possible
    3.  Unit test
    4.  External Api Docs
    5.  Vuln scanning as part of CI/CD other than `vuln-cost` plugin
12. Will be using standard full-fat mainline LTS Debian type containers and not slim/hardened Alpine like containers to facilitate debugging and hence save time
13. Node lib vulns are scanned automatically in my vscode by the `Vuln-cost` plugin 😊
14. Won't do any service health-checks/readiness probes
15. Just use plain un-encrypted protocols between containers (no https/mongo tls)
16. No advanced logging libs, just straight up `console.log` to `stdout`
17. No advanced input validation from APIs and stuff
18. Keeping network in `docker-compose.yml` simple and flat
19. No advanced error checking and graceful degradation
20. I will not version the containers like I normally would, we'll just go with `latest`
21. Containers will be built locally not via a build service
22. Would normally use multi-stage Docker builds with alpine to keep things small and secure, not here.
23. No secrets stored at all at the moment, so just committed `.env` file here
24. Each service sits in its own namespace

## TODO

In order:
- [x] create arch-diagram ADR
- [x] Create docker-compose.yaml to get all services created and tested locally
- [x] Create Worker B which will get data and write to Mongo - [`2h 11m so far`]
- [x] create API C - [`3h so far`]
  - [x] route `/` to get last latest value
  - [x] route `/average` to get average last 10 values
  - [x] route `/healthcheck` which doesn't hit our DB and create load
- [x] create front end A - [`3.5hrs so far`]
- [x] test all locally - [`4hrs so far`]
- [x] write k8s manifests test locally with `kind` and then on existing prod cluster
  - [x] database-d
  - [x] api-c
  - [x] worker-b - [`5hrs so far`]
  - [x] frontend-a
  - [x] test end-2-end using port-forward - [`5hrs 20mins so far`]
- [x] create k8s cluster deployment in TF
  - [ ] **OPTIONAL** create dedicated VPC first
  - [x] test infra end-2-end - [`5hrs 50mins so far`]
- [x] deploy nginx ingress
  - [x] wire up DNS for [https://test.jujhar.com]() to nginx ingress ip - [`6h so far`]
- [x] deploy application to k8s
  - [x] test end-2-end using port-forward
  - [x] ingress for frontend-a and api-c
  - [x] test end-2-end using domain name [`7hrs so far`]
  - [ ] work out RBAC
  - [ ] test end-2-end using an ubuntu container in namespace to test RBAC
- [ ] **OPTIONAL:** hook up nginx reverse-proxy in front of "Api C"
- [x] **OPTIONAL:** Replace wonky and sparse architecture diagram with better drawn one
- [ ] **BUG** fix multiple connections to mongoDb in API
- [x] **BUG** check sort is working in Api C

---

## Deployment

### Application Deployment

To build our containers and push to docker hub

```bash
# To deploy the application via k8s (just tested locally atm).  Tested on Ubuntu 20
# assuming you have kind/minikube/k3s setup locally and kubectl and docker installed
# assuming you have access to a docker hub registry to replace into the REGISTRY env var
REGISTRY=jujhars13 ./deployApp.sh
```

### Infra Deployment

TODO

---

## Development
### Running locally for dev

You'll need docker & docker-compose working on your machine.

To run simply type the following in the root:

```bash
docker-compose up --build

# test
curl http://localhost
```

## _scratch

Scratch area, so you can see and stuff I used to get the task done.

```bash
watch 'mongo mongodb://127.0.0.1:27017/bitcoin --eval "db.rate.find({})"'

# worker
# To test and run worker-b outside of docker-compose
docker build -t worker-b ./src/worker-b && \
docker run -it -v ${PWD}/src/worker-b/app:/app --env-file=.env -e MONGO_HOST=127.0.0.1 --network=host worker-b
(cd src/worker-b/app && npm install)

# api
(cd src/api-c/app && npm install)

docker build -t api-c ./src/api-c && \
docker run -it -v ${PWD}/src/api-c/app:/app --env-file=.env -e MONGO_HOST=127.0.0.1 --network=host -p 81:8080 api-c

curl localhost:81
watch "curl --silent localhost | jq"
inotify curl --silent localhost:81

# k8s-stuff
./deployApp.sh
watch kubectl ns
# debugging api
kubectl -n api-c get pods
kubectl -n api-c describe pod api-c-795495684b-7m7db
kubectl -n api-c logs -f api-c-795495684b-7m7db
kubectl -n api-c port-forward api-c-svc 8090:8080
curl -v localhost:8090/healthcheck
curl -v localhost:8090

# watch worker cronjobs
watch kubectl -n worker-b get pods

# check website
kubectl -n frontend-a port-forward svc/frontend-a-svc 8100:80
curl localhost:8100

# blast and start again to test app deployment
kubectl delete ns/{worker-b,api-c,database-d,frontend-a}
./deployApp.sh
kubectl -n frontend-a port-forward svc/frontend-a-svc 8100:80
curl localhost:8100
```

My terminal during development:
![my terminal during development](doc/terminal-screenshot.png)
![my terminal during k8s development](doc/terminal-screenshot-k8s.png)
