#!/usr/bin/env bash
# deploy infra for our application

if ! which helm; then
    >&2 echo "helm not installed"
    exit 22
fi

declare -r project=${GCP_PROJECT:-"seva-169521"}
declare -r clusterName=${CLUSTER_NAME:-"sneaky-cluster"}
declare -r region=${REGION:-"europe-west1"} # belgium

# gcloud auth application-default login
gcloud config set project "${project}"
echo "check to see if I can connect to GCP project"

if ! gcloud compute instances list ; then
	>&2 echo "error with gcloud authn or authz"
	exit 1
fi

echo "Running terraform" # in a subshell
(
    cd infra && \
    terraform init && \
    terraform apply \
        -auto-approve \
        -var="project=${project}" \
        -var="region=${region}" \
        -var="cluster_name=${clusterName}"
)

echo "get cluster deets"
gcloud container clusters get-credentials \
    "${CLUSTER_NAME}" \
    --region "${REGION}" \
    --project "${GCP_PROJECT}"

kubectl get nodes

echo -e "\nDeploying nginx ingress - without helm"
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud/deploy.yaml
# ensure there is at least one ingress per node, probably better off with a daemonset
kubectl -n ingress-nginx scale deployment/ingress-nginx-controller --replicas=4

# helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx/
# helm install --name ingress-nginx ingress-nginx/ingress-nginx

# get ip
nginxIngressAddress=$(kubectl -n ingress-nginx get svc ingress-nginx-controller \
    -o jsonpath='{.status.loadBalancer.ingress[].ip}')
echo -e "\n Remember to setup your url dns to point to ${nginxIngressAddress}"
# TODO wire up to cloudflare using cli
