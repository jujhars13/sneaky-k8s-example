#!/usr/bin/env bash
# deploy our application

# supply your own Docker registry, or use mine by default - assuming it's me running this

declare -r registry=${REGISTRY:-"jujhars13"}
# get a homebrew version number, will help force k8s manifest deployment updates
# TODO move out to GCP bucket rather than local file
if  [[ ! -f ".deployVersion" ]]; then
    echo 1 > .deployVersion
    version=1
else
    version=$(< .deployVersion)
    # increment version number ready for next time
    echo $((version+1)) > .deployVersion
fi
echo "Deploying version ${version} of our services"

< deploy/k8s/database-d.yml \
    kubectl apply -f -

##### Build and Deploy Worker b
# build container and push to dockerhub to the supplied registry
docker build -t "${registry}/worker-b:${version}" ./src/worker-b
docker push "${registry}/worker-b:${version}"
# inject in container name on the fly (use full url to denote it's not some private registry)
< deploy/k8s/worker-b.yml \
    sed "s/\$CONTAINER_NAME/docker.io\/${registry}\/worker-b:${version}/g" | \
    sed "s/\$VERSION/${version}/g" | \
    kubectl apply -f -

##### Build and Deploy Api C
# build container and push to dockerhub
docker build -t "${registry}/api-c:${version}" ./src/api-c
docker push "${registry}/api-c:${version}"
# inject in container name on the fly (use full url to denote it's not some private registry)
< deploy/k8s/api-c.yml \
    sed "s/\$CONTAINER_NAME/docker.io\/${registry}\/api-c:${version}/g" | \
    sed "s/\$VERSION/${version}/g" | \
    kubectl apply -f -

##### Build and Deploy Frontend A
# build container and push to dockerhub
# NB we have to use a separate Dockerfile because of our CORS proxy hack
docker build -t "${registry}/frontend-a:${version}" ./src/frontend-a -f ./src/frontend-a/Dockerfile.prod
docker push "${registry}/frontend-a:${version}"
# inject in container name on the fly (use full url to denote it's not some private registry)
< deploy/k8s/frontend-a.yml \
    sed "s/\$CONTAINER_NAME/docker.io\/${registry}\/frontend-a:${version}/g" | \
    sed "s/\$VERSION/${version}/g" | \
    kubectl apply -f -
